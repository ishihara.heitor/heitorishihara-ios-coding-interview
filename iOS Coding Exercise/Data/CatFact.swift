struct CatFact: Codable, Identifiable, Hashable {
    let id: String
    let text: String
    let type: String

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case text
        case type
    }
}
