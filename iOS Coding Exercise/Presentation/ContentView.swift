import SwiftUI

struct ContentView: View {
    // MARK: - Properties

    @ObservedObject private var viewModel = ContentViewModel()

    // MARK: - Body
    var body: some View {
        NavigationView {
            VStack {
                // MARK: - Cat Facts List
                Text("🐈")

                switch viewModel.state {
                case .loading:
                    ProgressView()
                        .onAppear {
                            viewModel.fetchData()
                        }
                case .normal(let catFacts):
                    List(catFacts) { fact in
                        HStack {
                            NavigationLink {
                            CatFactDetailView(catFact: fact)
                            } label: {
                                Text(fact.text)
                            }
                            Button(action: {
                                print("Favorited")
                            }, label: {
                                Image(systemName: "heart")
                            })
                            .buttonStyle(.plain)
                        }
                    }
                }
            }
            .navigationTitle("Cat Facts")
        }
    }


}

// MARK: - Preview
struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
