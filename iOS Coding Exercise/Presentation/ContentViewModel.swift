import SwiftUI

final class ContentViewModel: ObservableObject {

    enum State {
        case loading
        case normal([CatFact])
    }

    private var catFacts: [CatFact] = [] // CatFact is your model for the response
    private var selectedCatFact: CatFact? = nil

    @Published private(set) var state: State = .loading

    func favorited() {

    }

    // MARK: - Fetch Data
    func fetchData() {
        guard let url = URL(string: "https://cat-fact.herokuapp.com/facts") else { return }
        URLSession.shared.dataTask(with: url) { data, _, error in
            if let data = data {
                do {
                    let catFacts = try JSONDecoder().decode([CatFact].self, from: data)
                    DispatchQueue.main.async {
                        self.catFacts = catFacts
                        self.state = .normal(catFacts)
                    }
                } catch {
                    print("Error decoding JSON: \(error)")
                }
            } else if let error = error {
                print("Error fetching data: \(error)")
            }
        }
        .resume()
    }
}
